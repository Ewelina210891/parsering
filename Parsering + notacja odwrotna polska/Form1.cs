﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Parsering
{
    public partial class Form1 : Form
    {
        private string _expression;
        private string _result = "";
        private string _name = "";
        private int _i;
        private int _j;
        private List<Symbol> _symbols;
        private bool _error;
        private Symbol _actualSymbol;
        private int _index;
        private string _resultMessage;
        private Stack<double> stosLiczb = new Stack<double>();
        public Form1()
        {
            InitializeComponent();
        }

        private void BtnLoadFileClick(object sender, EventArgs e)
        {
            txtExpression.Clear();
            txtNotation.Clear();
            txtKoniec.Clear();
            lbResult.Text = "Podaj wyrażenie";
            _symbols = new List<Symbol>();
            btnAnalize.Enabled = false;

            var dialog1 = new OpenFileDialog {Filter = "txt file (*.txt)|*.txt"};
            if (dialog1.ShowDialog() == DialogResult.OK)
            {
                var reader = new StreamReader(dialog1.FileName);
                _expression = reader.ReadLine();
                if (_expression != null && CheckExpression(_expression))
                {
                    txtExpression.Text = _expression;
                    btnAnalize.Enabled = true;
                    lbResult.Text = "Analiza leksykalna: ok. Wykonaj analizę.";
                }
                else lbResult.Text = "Analiza leksykalna: błąd! Podaj inne wyrażenie!";

                reader.Close();
            }
        }

        private bool CheckExpression(string expression)
        {
            _j = 0;
            while (_j < expression.Length)
            {
                _i = _j;
                if (char.IsNumber(expression[_i]))
                {
                    _name = "liczba całkowita";
                    _result = "";
                    Number(_i, expression);
                    _symbols.Add(new Symbol
                                     {
                                         Name = _name,
                                         FullSymbol = _result
                                     });
                }
                else
                {
                    //if (char.IsLetter(expression[_i]) || expression[_i] == char.Parse("_"))
                    //{
                    //    _name = "identyfikator";
                    //    _result = "";

                    //    Letter(_i, expression);
                    //    _symbols.Add(new Symbol
                    //                     {
                    //                         Name = _name,
                    //                         FullSymbol = _result
                    //                     });
                    //}
                    //else
                    //{
                        if (expression[_i] == char.Parse(" ") || expression[_i] == char.Parse("\t") ||
                            expression[_i] == char.Parse("\n"))
                        {
                            _j++;
                        }
                        else
                        {
                            switch (expression[_i])
                            {
                                case '+':
                                    _symbols.Add(new Symbol
                                                     {
                                                         Name = "plus",
                                                         FullSymbol = "+"
                                                     });
                                    _j++;
                                    break;
                                case '-':
                                    _symbols.Add(new Symbol
                                                     {
                                                         Name = "minus",
                                                         FullSymbol = "-"
                                                     });
                                    _j++;
                                    break;
                                case '*':
                                    _symbols.Add(new Symbol
                                                     {
                                                         Name = "znak mnożenia",
                                                         FullSymbol = "*"
                                                     });
                                    _j++;
                                    break;
                                case '/':
                                    _symbols.Add(new Symbol
                                                     {
                                                         Name = "znak dzielenia",
                                                         FullSymbol = "/"
                                                     });
                                    _j++;
                                    break;
                                case '(':
                                    _symbols.Add(new Symbol
                                                     {
                                                         Name = "nawias lewy",
                                                         FullSymbol = "("
                                                     });
                                    _j++;
                                    break;
                                case ')':
                                    _symbols.Add(new Symbol
                                                     {
                                                         Name = "nawias prawy",
                                                         FullSymbol = ")"
                                                     });
                                    _j++;
                                    break;
                                default:
                                    return false;
                            }
                        }
                    }
                }
            //}
            _i = 0;
            return true;
        }

        private void Number(int i, string expr)
        {
            if (expr.Length > i && char.IsNumber(expr[i]))
            {
                _result += expr[i];
                i++;
                _j = i;
                Number(i, expr);
            }
            else
            {
                if (expr.Length > i && expr[i] == char.Parse(".")) 
                {
                    if (_result.Length != 0)
                    {
                        if (_result.Any(c => c == char.Parse(".")))
                        {
                            return;
                        }
                        _result += expr[i];
                        i++;
                        _j = i;
                        _name = "liczba zmiennoprzecinkowa";
                        Number(i, expr);
                    }
                }
            }
        }

        private void Letter(int i, string expr)
        {
            if (expr.Length > i)
            {
                if (expr.Length > i && (char.IsLetter(expr[i]) || expr[i] == char.Parse("_")))
                {
                    _result += expr[i];
                    i++;
                    _j = i;
                    Letter(i, expr);
                }
                else
                {
                    if (expr.Length > i && char.IsNumber(expr[i]))
                    {
                        _result += expr[i];
                        i++;
                        _j = i;
                        Letter(i, expr);
                    }
                }
            }
        }

        private void BtnAnalizeClick(object sender, EventArgs e)
        {
            _index = 0;
            _resultMessage = "Wyrażenie spełnia gramatykę.";
            _error = false;

            _actualSymbol = NextSymbol();
            W();

            lbResult.Text = _index > _symbols.Count ? _resultMessage : "Błąd! Brak odpowiedniego symbolu";

            var wynikKoncowy = stosLiczb.Pop();
            if (wynikKoncowy != null) txtKoniec.Text = wynikKoncowy.ToString();

        }
       
        private Symbol NextSymbol()
        {
            ++_index;

            return _index <= _symbols.Count ? _symbols[_index - 1] : new Symbol { Name = "end of data" };
            
        }

        private void Loading(string symbol)
        {
            if (!_error)
            {
                if (_actualSymbol.Name == symbol && _index <= _symbols.Count)
                    _actualSymbol = NextSymbol();
                else
                {
                    _resultMessage = "Błąd!";
                    _error = true;
                }
            }
        }

        private void W()
        {
            if (_error) return;

            S();
            W2();
        }

        private void W2()
        {
            if (_error) return;

            if (_actualSymbol.Name == "plus")
            {
                Loading("plus");
                S();
                Dodawanie();
                W2();
                
            }

            if (_actualSymbol.Name == "minus")
            {
                
                Loading("minus");
                S();
                Odejmowanie();
                W2();
                
            }
        }

        private void S()
        {
            if (_error) return;

            C();
            S2();
        }

        private void S2()
        {
            if (_error) return;

            if (_actualSymbol.Name == "znak mnożenia")
            {
                Loading("znak mnożenia");
                C();
                Mnożenie();
                S2();
                
            }
            if (_actualSymbol.Name == "znak dzielenia")
            {
                Loading("znak dzielenia");
                C();
                Dzielenie();
                S2();
                
            }
        }

        private void C()
        {
            if (_error) return;

            switch (_actualSymbol.Name)
            {
                case "liczba całkowita":
                    Liczba(_actualSymbol);
                    Loading("liczba całkowita");
                    
                    break;
                case "liczba zmiennoprzecinkowa":
                    Liczba(_actualSymbol);
                    Loading("liczba zmiennoprzecinkowa");
                    
                    break;
                //case "identyfikator":
                //    Loading("identyfikator");
                //    break;
                default:
                    Loading("nawias lewy");
                    W();
                    Loading("nawias prawy");
                    break;
            }
        }

        private void Dodawanie()
        {
            var liczba1 = stosLiczb.Pop();
            var liczba2 = stosLiczb.Pop();
            var wynik = liczba2 + liczba1;
            stosLiczb.Push(wynik);

            txtNotation.Text += "+ ";
        }
        private void Odejmowanie()
        {
            var liczba1 = stosLiczb.Pop();
            var liczba2 = stosLiczb.Pop();
            var wynik = liczba2 - liczba1;
            stosLiczb.Push(wynik);

            txtNotation.Text += "- ";
        }

        private void Mnożenie()
        {
            var liczba1 = stosLiczb.Pop();
            var liczba2 = stosLiczb.Pop();
            var wynik = liczba2 * liczba1;
            stosLiczb.Push(wynik);

            txtNotation.Text += "* ";
        }

        private void Dzielenie()
        {
            var liczba1 = stosLiczb.Pop();
            var liczba2 = stosLiczb.Pop();
            if (liczba2 != 0)
            {
                var wynik = liczba2 / liczba1;
                stosLiczb.Push(wynik);
                txtNotation.Text += "/ ";
            }
            else
            {
                _error = true;
                txtNotation.Text = "";
                lbResult.Text = "Błąd! Dzielenie przez zero!";
            }
        }

        private void Liczba(Symbol actualSymbol)
        {
            var liczba = Convert.ToDouble(actualSymbol.FullSymbol);
            txtNotation.Text += liczba + " ";
            stosLiczb.Push(liczba);
        }





        
    }  
}