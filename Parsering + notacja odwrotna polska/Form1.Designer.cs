﻿namespace Parsering
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLoadFile = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtExpression = new System.Windows.Forms.TextBox();
            this.lbResult = new System.Windows.Forms.Label();
            this.btnAnalize = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNotation = new System.Windows.Forms.TextBox();
            this.txtKoniec = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnLoadFile
            // 
            this.btnLoadFile.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnLoadFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnLoadFile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLoadFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnLoadFile.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnLoadFile.Location = new System.Drawing.Point(12, 12);
            this.btnLoadFile.Name = "btnLoadFile";
            this.btnLoadFile.Size = new System.Drawing.Size(238, 44);
            this.btnLoadFile.TabIndex = 0;
            this.btnLoadFile.Text = "Wczytaj wyrażenie z pliku";
            this.btnLoadFile.UseVisualStyleBackColor = false;
            this.btnLoadFile.Click += new System.EventHandler(this.BtnLoadFileClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(12, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "WYRAŻENIE: ";
            // 
            // txtExpression
            // 
            this.txtExpression.Enabled = false;
            this.txtExpression.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtExpression.Location = new System.Drawing.Point(12, 103);
            this.txtExpression.Name = "txtExpression";
            this.txtExpression.Size = new System.Drawing.Size(370, 29);
            this.txtExpression.TabIndex = 2;
            // 
            // lbResult
            // 
            this.lbResult.AutoSize = true;
            this.lbResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbResult.ForeColor = System.Drawing.Color.Red;
            this.lbResult.Location = new System.Drawing.Point(11, 199);
            this.lbResult.Name = "lbResult";
            this.lbResult.Size = new System.Drawing.Size(163, 24);
            this.lbResult.TabIndex = 4;
            this.lbResult.Text = "Podaj wyrażenie";
            // 
            // btnAnalize
            // 
            this.btnAnalize.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnAnalize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAnalize.Enabled = false;
            this.btnAnalize.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnAnalize.ForeColor = System.Drawing.SystemColors.Control;
            this.btnAnalize.Location = new System.Drawing.Point(12, 147);
            this.btnAnalize.Name = "btnAnalize";
            this.btnAnalize.Size = new System.Drawing.Size(103, 40);
            this.btnAnalize.TabIndex = 5;
            this.btnAnalize.Text = "Analizuj";
            this.btnAnalize.UseVisualStyleBackColor = false;
            this.btnAnalize.Click += new System.EventHandler(this.BtnAnalizeClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(13, 232);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(306, 18);
            this.label2.TabIndex = 6;
            this.label2.Text = "Wyrażenie w notacji odwrotnej polskiej:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(13, 306);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 18);
            this.label3.TabIndex = 7;
            this.label3.Text = "Wynik:";
            // 
            // txtNotation
            // 
            this.txtNotation.Enabled = false;
            this.txtNotation.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtNotation.Location = new System.Drawing.Point(12, 265);
            this.txtNotation.Name = "txtNotation";
            this.txtNotation.Size = new System.Drawing.Size(370, 29);
            this.txtNotation.TabIndex = 8;
            // 
            // txtKoniec
            // 
            this.txtKoniec.Enabled = false;
            this.txtKoniec.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtKoniec.Location = new System.Drawing.Point(12, 327);
            this.txtKoniec.Name = "txtKoniec";
            this.txtKoniec.Size = new System.Drawing.Size(374, 29);
            this.txtKoniec.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.CausesValidation = false;
            this.ClientSize = new System.Drawing.Size(398, 367);
            this.Controls.Add(this.txtKoniec);
            this.Controls.Add(this.txtNotation);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnAnalize);
            this.Controls.Add(this.lbResult);
            this.Controls.Add(this.txtExpression);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnLoadFile);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLoadFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtExpression;
        private System.Windows.Forms.Label lbResult;
        private System.Windows.Forms.Button btnAnalize;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNotation;
        private System.Windows.Forms.TextBox txtKoniec;
    }
}

